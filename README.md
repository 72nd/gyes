# gyes − Framework for fast MIDI and OSC application development

Gyes tries to take care of the most common problems, boilerplate and design patterns occurring when developing applications for [Open Sound Control (OSC)](http://opensoundcontrol.org/spec-1_0) or [MIDI](https://en.wikipedia.org/wiki/MIDI). Gyes provides a wide array of functionality to send and receive OSC/MIDI commands. For the receiving of messages, a system hook based system is used to define certain events (input from OSC or MIDI) when a function should be triggered. This is especially useful when trying to connect the world of OSC and MIDI together.

## Installation

```shell script
apt install python-dev libasound2-dev libjack-jackd2-dev

git clone https://gitlab.com/72nd/gyes.git
cd gyes
python3 -m venv init venv
source venv/bin/activate
python3 setup.py install
pip3 install -r requirements
```

## Example

This example derives from `furore.py`:

```python
from gyes import Gyes, OSC
from gyes.data import Values
from gyes.osc import OSCMonitor

def callback(address: str, values: Values):
    print("got address {} with values {}".format(str, values))

def simple_callback():
    print("got a new message")

# If the given regex contains groups, they will given to the function in matches.
def regex_callback(address, values, matches):
    print("regex: address «{}», matches: {}".format(address, matches))

if __name__ == "__main__":
    mo = Gyes()
    osc = OSC(
        host="127.0.0.1",
        port_in=9000,
        dst_host="127.0.0.1",
        dst_port=9010
    )
    osc.add_hook("/empty", callback)
    # The callback can also contain no arguments.
    osc.add_hook("/float", simple_callback, values=[0.0, 1.0])
    osc.add_regex_hook("/player/(.*)/play", regex_callback)
    mo.add_osc(osc)

    # Gyes provides also a simple OSC monitor which shows all incoming messages.
    osc2 = OSCMonitor(
        port_in=9001
    )
    mo.add_osc(osc2)
```

