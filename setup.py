from distutils.core import setup
from setuptools import find_packages

setup(
    name='gyes',
    author='72nd',
    version='0.1',
    description='MIDI − OSC framework',
    packages=find_packages(),
    include_package_data=True
)
