from __future__ import annotations

import inspect
import logging
from typing import Union, List, Tuple, NewType, Optional, Callable, Any, Dict, Type

ValuesData = NewType('ValuesData', List[Union[int, float, bytes, str, bool]])
InputValues = NewType('InputValues', Optional[Union[int, float, bytes, str, bool, tuple, list]])


class Values(object):
    """Represents values which can be sent and received via OSC messages.

    Args:
        data: List of OSC values which the incoming message has to match. The ordering is not taken into account.
    """
    __data: ValuesData

    def __init__(self, data: ValuesData):
        self.__data = data

    @staticmethod
    def from_input_values(input_values: InputValues) -> Values:
        """For convince reasons the framework user can add hooks with only one argument without stating any tuples and
        lists. This function converts the input into a Values instance.

        Args:
            input_values: The input values from the framework user.

        Returns:
            A Values instance according to the input.
        """
        if isinstance(input_values, list):
            return Values(Values.__list_to_values_data(input_values, False))
        if not isinstance(input_values, (int, float, bytes, str, bool)):
            logging.error("the value «%s» not a valid type (int, float, bytes, str, bool)", input_values)
            return Values([])
        return Values([input_values])

    @staticmethod
    def from_python_osc(python_osc_data: Tuple[List, ...], round_floats: bool) -> Values:
        """Builds and returns an Values object from an incoming python-osc message values.

        Args:
            python_osc_data: Values originated in a python-osc message.
            round_floats: States whether the input floats should be rounded at the 8th decimal place.

        Returns:
            A Values instance according to the input.
        """
        return Values(Values.__list_to_values_data(python_osc_data, round_floats))

    @staticmethod
    def __list_to_values_data(values: Union[List, Tuple], round_floats: bool) -> ValuesData:
        data: ValuesData = []
        for entry in values:
            if not isinstance(entry, (int, float, bytes, str, bool)):
                logging.error("the value «%s» from the message values «%s» has not a valid type (int, float, bytes,"
                              "str, bool)", entry, values)
                continue
            if round_floats and isinstance(entry, float):
                data.append(round(entry, 8))
                continue
            data.append(entry)
        return data

    def __str__(self) -> str:
        result = ""
        is_first = True
        for entry in self.__data:
            if is_first:
                result = "{} ({})".format(str(entry), type(entry).__name__)
                is_first = False
            else:
                result = "{}, {} ({})".format(result, str(entry), type(entry).__name__)
        return "[{}]".format(result)

    def __len__(self) -> int:
        return len(self.__data)

    def __getitem__(self, key: int) -> Values:
        return self.__data[key]

    def equal(self, other: Values, enforce_value_order: bool) -> bool:
        """Tests if given values are equal to the instance data.

        Args:
            other: Values to be tested.
            enforce_value_order: States whether when comparing with other Values the order has to be the same.

        Returns:
            True if a both values are the same, False otherwise.
        """
        # If both are empty, they are the same.
        if len(self.__data) == 0 and len(other) == 0:
            return True
        # Could not be equal if not the same length.
        if len(self.__data) != len(other):
            return False
        # When value order is enforced, test this.
        if enforce_value_order:
            for i in range(len(self.__data)):
                if self.__data[i] != other[i]:
                    return False
            return False
        return len(set(self.__data).intersection(set(other))) == len(self.__data)


class Callback(object):
    """Represents a callback function for gyes hooks. Provides the ability to define the type of functions which can be
    executed.

     Args:
        function: Function to be called on callback.
        args: A tuple of a tuple with the allowed args.
    """
    __function: Callable
    __args: Dict[Dict[Type]]
    __args_type: str

    def __init__(self, function: Callable, args: Dict[Dict[Type]]):
        self.__function = function
        self.__args = args
        self.__args_type = self.get_args_dict_name(function, args)

        if self.__args_type is None:
            raise TypeError("The args of given callback function «{}» does not meet the criteria: {}".format(
                function.__name__, self.args_str(args)))

    def call(self, args_type_name: str, *args):
        """Calls the contained function."""
        # ToDo: Rewrite documentation.
        if self.__args_type != args_type_name:
            return
        self.__function(*args)

    @staticmethod
    def get_args_dict_name(function: Callable, args: Dict[Dict[Type]]) -> Optional[str]:
        """Searches for a existing args combination and returns it. When no valid combination is found ``None`` is
        returned.

        Args:
            function: Function to be called on callback.
            args: A tuple of a tuple with the allowed args.

        Returns:
            Args combination type name. When no matching dict is found: ``None``.
        """
        params = inspect.signature(function).parameters
        for possibility in args:
            if len(params) == len(args[possibility]):
                is_same_j = True
                i = 0
                lst = list(params.values())
                for arg in args[possibility]:
                    if args[possibility][arg] != lst[i].annotation and lst[i].annotation != inspect.Signature.empty:
                        is_same_j = False
                        break
                    i += 1
                if is_same_j:
                    return possibility
        return None

    @staticmethod
    def args_str(args: Dict[Dict[Type]]) -> str:
        """Human readable version of args."""
        result = ""
        first_i = True
        for possibility in args:
            result_j = ""
            if len(args[possibility]) != 0:
                first_j = True
                for arg in args[possibility]:
                    if first_j:
                        result_j = "{}: {}".format(arg, args[possibility][arg].__name__)
                        first_j = False
                    else:
                        result_j = "{}, {}: {}".format(result_j, arg, args[possibility][arg].__name__)
            if first_i:
                result = "[{}]".format(result_j)
                first_i = False
            else:
                result = "{} or [{}]".format(result, result_j)
        return result
