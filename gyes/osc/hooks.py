import logging
import re
from typing import NewType, Optional, Tuple, Callable, Any, List, Dict, Type

from gyes.data import Values, Callback

OptValues = NewType('OptValues', Optional[Values])
AddressAndValues = NewType('AddressAndValues', Tuple[str, OptValues])


class OscHook(object):
    """Represents a hook which is called when a incoming OSC message matches the hooks conditions.

    This base class provides the fundamental function for OSC hooks and will be sufficient for the most use cases.
    The matching of incoming addresses is done as defined in the `OSC Specification
    <http://opensoundcontrol.org/spec-1_0>`_. This means, wildcards (``*``) and so on can be used.

    It's also possible to define the nature of the values which have to match up with the incoming OSC message.

    Args:
        address: The address the OSC message which has to match up with the incoming command. Pattern matching as
            defined in the OSC standard is possible.
        values: A range of values the incoming message has to match up for the hook.
        callback: Function which should be called, when the incoming message matches the hook.
        enforce_values_order: States whether when comparing with other Values the order has to be the same.
        round_floats: States whether the input floats should be rounded at the 8th decimal place.
    """

    address: str
    values: Values
    _callback: Callback
    _allowed_callback_types = {
        "SimpleOSC": {},
        "DefaultOSC": {"address": str, "values": Values}
    }
    _enforce_values_order: bool
    round_floats: bool
    """States whether the default address matching from python-osc should be used."""

    def __init__(self, address: str, values: Values, callback: Callable, enforce_values_order: bool,
                 round_floats: bool):
        self.address = address
        self.values = values
        self._callback = Callback(callback, self._allowed_callback_types)
        self._enforce_values_order = enforce_values_order
        self.round_floats = round_floats

    def execute_when_match(self, msg_address: str, msg_values: Values) -> bool:
        """Asses the given address an values. When matching the on_hook function will be called. This function should be
        called, when no further distinction elsewhere is possible.

        Args:
            msg_address: OSC address of the incoming message.
            msg_values: OSC values of the incoming message.

        Returns:
            States whether the the hook was fired or not.
        """
        if not self._match_address(msg_address):
            return False
        if not self._match_arguments(msg_values):
            logging.info(
                "received message with address «%s» has different arguments than expected (expected: %s, received: %s)",
                msg_address, self.values, msg_values)
            return False
        logging.debug("got new matching message with address «%s» and argument(s) «%s»", msg_address, msg_values)
        self._callback.call("SimpleOSC")
        self._callback.call("DefaultOSC", msg_address, msg_values)
        return True

    def _match_address(self, address: str) -> bool:
        """Returns whether the given address matches the address of the hook.

        Args:
            address: OSC address to be checked.

        Returns:
            True if the addresses are the same, otherwise False.
        """
        return self.address == address

    def _match_arguments(self, values: Values):
        """Matches the given arguments against the argument of the hook.

        In the basic class, this method doesn't do much (it just compares the two values). But it can be important for
        more advanced hooks which derived from this class..

        Args:
            values: Some OSC values to be tested against the hook.

        Returns:
            True if given arguments match, False otherwise.
        """
        return self.values.equal(values, self._enforce_values_order)


class OscRegexHook(OscHook):
    """Extends OscHook by the possibility to define OSC addresses as regular expressions.

    Args:
        address_expr: OSC address which can contain regular expressions.
        values: A range of values the incoming message has to match up for the hook. The matching algorithm of the
            parent class (:func:`~gyes.osc.hooks.OscHook.match_arguments`) is used.
        callback: Function which should be called, when the incoming message matches the hook.
    """

    _allowed_callback_types = {
        "SimpleOSC": {},
        "DefaultOSC": {"address": str, "values": Values},
        "RegexOSC": {"address": str, "values": Values, "matches": List[str]}
    }

    def __init__(self, address_expr: str, values: Values, callback: Callable[[Any], None], enforce_values_order: bool,
                 round_floats: bool):
        super(OscRegexHook, self).__init__(address_expr, values, callback, enforce_values_order, round_floats)

    def _match_address(self, address: str) -> bool:
        return bool(re.search(self.address, address))

    def execute_when_match(self, msg_address: str, msg_values: Values) -> bool:
        """Asses the given address an values. When matching the on_hook function will be called. This function should be
        called, when no further distinction elsewhere is possible.

        Args:
            msg_address: OSC address of the incoming message.
            msg_values: OSC values of the incoming message.

        Returns:
            States whether the the hook was fired or not.
        """
        super(OscRegexHook, self).execute_when_match(msg_address, msg_values)
        self._callback.call("RegexOSC", msg_address, msg_values, re.findall(self.address, msg_address))
        return True
