from typing import List


class Error(Exception):
    """Base class for custom OSC exceptions."""
    pass


