import asyncio
import logging
import time
from typing import Optional, List, Callable, Any

from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.udp_client import SimpleUDPClient

from gyes.data import Values, InputValues
from gyes.osc.hooks import OscHook, AddressAndValues, OscRegexHook


class OSC:
    """Provides functionality to work with the `Open Sound Control (OSC) <http://opensoundcontrol.org/spec-1_0>`_
    protocol. Hint: Can be imported with ``gyes.OSC``.

    In most cases it's advisable to define one OSC instance per device. The arguments can be set to ``None``, if some of
    the function is not needed. To such an instance hooks can be added to define some action triggered when a certain
    OSC message arrives. Beside the normal matching against some address, it's also possible to check if a certain
    combination of OSC values is received. Further more specialised hooks are available for more advanced address
    matching (for example: regex).

    Args:
        port_in: Port of the OSC server to listen on thus the receiving port. Can be set to ``None`` when using the
            instance only for sending messages (thus not starting an OSC server).
        dst_host: Address/IP of the destination device. Can be set to ``None`` when using the instance only for
            receiving messages (aka just as an OSC server).
        dst_port: Port where the OSC messages should be send to on the destination device. Can be set to ``None`` when
            using the instance only for receiving messages (aka just as an OSC server).
        host: Address/IP of the host where the OSC server runs. An OSC server listens for new messages. This is almost
            always localhost thus has ``127.0.0.1`` as default value.
        wait_between_sending: Defines the waiting time in milli seconds (ms) between the sending of OSC messages.
            This can be important when sending bulk messages and the order of receiving has to be the correct.
            Default is 15ms.
    """

    _host: str
    _port_in: Optional[int]
    __dst_host: Optional[str]
    __dst_port: Optional[int]
    __wait_between: int
    __default_address_hooks: List[OscHook]
    __custom_address_hooks: List[OscHook]
    __fixed_arguments_length = 1
    """The dispatcher of python-osc allows to match incoming addresses against all registered ones while comply to the
    OSC matching standard. It doesn't make sense to rewrite this default behaviour. Thus when mapping with python-osc is
    done in :func:`~gyes.osc.device.OSC.__build_dispatcher` the hook itself is given as part of the fixed_argument list
    to python-osc. To check the sanity of given arguments in :func:`__on_message` (which is called by the python-osc
    server the variable states the desired length of the fixed arguments."""

    def __init__(self, port_in: Optional[int], dst_host: Optional[str], dst_port: Optional[int],
                 host: str = "127.0.0.1", wait_between_sending: int = 15):
        self._host = host
        self.__dst_host = dst_host
        self._port_in = port_in
        self.__dst_port = dst_port
        self.__wait_between = wait_between_sending
        self.__default_address_hooks: [OscHook] = []
        self.__custom_address_hooks: [OscHook] = []

    def add_hook(self, address: str, callback: Callable[[str], None], values: InputValues = None,
                 enforce_values_order: bool = False, round_floats: bool = True) -> None:
        """Register a hook with a function which will be called when a OSC message with a matching message (optional:
        matching values) is received.

        Args:
            address: The OSC address the hook as to match when it should fire. It's possible to use the OSC specified
                matching algorithms (learn more `here <http://opensoundcontrol.org/spec-1_0>`_) and use for example
                wildcards (``*``).
            callback: Function which should be called, when hook matches an incoming OSC message. The called address can
                be obtained by the first argument. This can be useful when using wildcards thus not knowing
                the whole address in advance.
            values: Optional list of OSC values which the incoming message has to match. The ordering is not taken into
                account.
            enforce_values_order: States whether the values of the incoming message must have the same order as defined
                (default is ``False``).
            round_floats: States whether the input floats should be rounded at the 8th decimal place (default is
                ``True``).

        Returns:
            Nothing.
        """
        if values is None:
            values = []
        self.__default_address_hooks.append(OscHook(
            address=address,
            values=Values.from_input_values(values),
            callback=callback,
            enforce_values_order=enforce_values_order,
            round_floats=round_floats
        ))

    def add_regex_hook(self, address: str, callback: Callable, values: InputValues = None,
                       enforce_values_order: bool = False, round_floats: bool = True) -> None:
        if values is None:
            values = []
        self.__custom_address_hooks.append(OscRegexHook(
            address_expr=address,
            values=Values.from_input_values(values),
            callback=callback,
            enforce_values_order=enforce_values_order,
            round_floats=round_floats
        ))

    async def serve(self) -> None:
        """Starts the OSC server to listen for new messages.

        Returns:
            Nothing.
        """
        srv = AsyncIOOSCUDPServer((self._host, self._port_in), self._build_dispatcher(), asyncio.get_event_loop())
        logging.info("OSC listens starts on %s:%d", self._host, self._port_in)
        transport, protocol = await srv.create_serve_endpoint()

    def _build_dispatcher(self) -> Dispatcher:
        """Builds the python-osc dispatcher for all hooks with default (OSC standard) address matching. It also appends
        a default handler (:func:`__default_osc_hook`) to get control for all special cases with custom address
        matching.

        Returns:
            A dispatcher according to the state of the object.
        """
        dsp = Dispatcher()
        dsp.set_default_handler(self.__default_osc_hook)

        for hook in self.__default_address_hooks:
            dsp.map(hook.address, self.__on_message, hook)
        return dsp

    def __default_osc_hook(self, address: str, *osc_arguments: List[Any]) -> None:
        """Function which is called by python-osc when no handler inside python-osc was defined. This is used to allow
        gyes hooks to define their own address matching mechanism via :func:`~gyes.osc.OscHook.match_address`.

        Args:
            address: OSC address.
            *osc_arguments: OSC arguments as a list.

        Returns:
            Nothing.
        """
        found_something = False
        for hook in self.__custom_address_hooks:
            converted_values = Values.from_python_osc(osc_arguments, hook.round_floats)
            did_run = hook.execute_when_match(address, converted_values)
            if not found_something and did_run:
                found_something = True
        if not found_something:
            logging.warning("No matching hook for OSC message with address «%s» and argument(s) «%s»",
                            address, str(osc_arguments))

    def __on_message(self, address: str, fixed_arguments: List[Any], *osc_arguments: List[Any]) -> None:
        # ToDo: I stopped here yesterday.
        if len(fixed_arguments) != self.__fixed_arguments_length:
            logging.fatal("number of fixed arguments invalid, expected: %d, actual: %d", self.__fixed_arguments_length,
                          len(fixed_arguments))
            return
        hook = fixed_arguments[0]
        if not isinstance(hook, OscHook):
            logging.fatal("first fixed argument is not of type Hook")
            return
        # Convert python-osc arguments to gyes OSC Values.
        converted_values = Values.from_python_osc(osc_arguments, hook.round_floats)
        hook.execute_when_match(address, converted_values)

    def send(self, address: str, values: Values = None) -> None:
        """Sends a OSC message to the destination host and port defined in the class instance.

        This mostly makes sense when you have to send a lot of messages to the same device/service. Otherwise there is
        the static function :func:`~send_osc` which can send an OSC message without an
        :any:`OSC` instance.

        Args:
            address: OSC address.
            values: OSC values.

        Returns:
            Nothing.
        """
        self.send_osc(self.__dst_host, self.__dst_port, address, values)

    def bulk_send(self, addresses: List[str]) -> None:
        self.bulk_send_osc(self.__dst_host, self.__dst_port, addresses, self.__wait_between)

    def bulk_send_with_values(self, bulk: List[AddressAndValues]) -> None:
        self.bulk_send_osc_with_values(self.__dst_host, self.__dst_port, bulk, self.__wait_between)

    @staticmethod
    def send_osc(dst_host: str, dst_port: int, address: str, values: Values = None):
        if values is None:
            values = []
        client = SimpleUDPClient(dst_host, dst_port)
        client.send_message(address, values)

    @staticmethod
    def bulk_send_osc(dst_host: str, dst_port: int, addresses: List[str], wait_between: int):
        for address in addresses:
            OSC.send_osc(dst_host, dst_port, address)
            if wait_between > 0:
                time.sleep(wait_between / 100)

    @staticmethod
    def bulk_send_osc_with_values(dst_host: str, dst_port: int, bulk: List[AddressAndValues], wait_between: int):
        for item in bulk:
            OSC.send_osc(dst_host, dst_port, item[0], item[1])
            if wait_between > 0:
                time.sleep(wait_between / 100)


class OSCMonitor(OSC):
    """Displays all incoming OSC messages for convenient debugging. A monitor instance can be started with the
    :func:`serve` function.

    Args:
        port_in: Port of the OSC server to listen on thus the receiving port. Can be set to ``None`` when using the
            instance only for sending messages (thus not starting an OSC server).
        host: Address/IP of the host where the OSC server runs. An OSC server listens for new messages. This is almost
            always localhost thus has ``127.0.0.1`` as default value.
    """

    def __init__(self, port_in: int, host: str = "127.0.0.1"):
        super(OSCMonitor, self).__init__(
            port_in=port_in,
            dst_host=None,
            dst_port=None,
            host=host
        )

    def _build_dispatcher(self) -> Dispatcher:
        """Overloading the parent method to relay all incoming messages to the :func:`__monitor_handler` function
        which will display the incoming traffic to the user.

        Returns:
            The dispatcher.
        """
        dsp = Dispatcher()
        dsp.map("*", self.__monitor_handler, None)
        return dsp

    def __monitor_handler(self, address: str, fixed_arguments: List[Any], *osc_arguments: List[Any]) -> None:
        """Displays the incoming messages to the user.

        Args:
            address: OSC address.
            fixed_arguments: List of fixed arguments. No usage in this case.
            *osc_arguments: OSC arguments as a list.

        Returns:
            Nothing.
        """
        print("New message on port {} with address «{}» and arguments {}".format(self._port_in, address, osc_arguments))
