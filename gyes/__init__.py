import asyncio
from typing import List

from .osc import OSC
from .midi import MIDI


class Gyes:
    """Main instance to attach OSC/MIDI hook groups."""
    def __init__(self):
        self.__osc_instances: List[OSC] = []
        self.__midi_instances: List[MIDI] = []

    def add_osc(self, osc_instance: OSC):
        """Add a OSC hook group.

        Args:
             osc_instance: OSC instance to be added.
        """
        self.__osc_instances.append(osc_instance)

    def add_midi(self, midi_instance: MIDI):
        """Add a MIDI hook group.

        Args:
            midi_instance: MIDI instance to be added.
        """
        self.__midi_instances.append(midi_instance)

    def serve(self):
        """Starts all the registered servers/listeners.

        Returns:
            Nothing.
        """
        loop = asyncio.get_event_loop()
        for osc_instance in self.__osc_instances:
            asyncio.ensure_future(osc_instance.serve())
        for midi_instance in self.__midi_instances:
            asyncio.ensure_future(midi_instance.serve())
        loop.run_forever()
