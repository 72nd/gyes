import logging
from typing import Optional

from .button import MidiButton

import mido


class MIDI:
    def __init__(self, input_name: str):
        self.__input_name = input_name
        self.__buttons: [MidiButton] = []

    def add_button(self, button: MidiButton):
        self.__buttons.append(button)

    def activate_by_name(self, name: str) -> None:
        pass

    def deactivate_by_name(self, name: str) -> None:
        pass

    def __get_button_by_name(self, name) -> Optional[MidiButton]:
        for button in self.__buttons:
            if button.name == name:
                return button
        return None

    async def serve(self) -> None:
        with mido.open_input(self.__input_name) as input_port:
            for msg in input_port:
                logging.info(msg)
