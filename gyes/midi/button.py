from enum import Enum
from typing import Optional, List, Callable, Any


class Color(Enum):
    FOO = 9
    BAR = 5


class Type(Enum):
    NOTE_ON = "note_on"
    CONTROL_CHANGE = "control_change"


class State:
    def __init__(self, color: Color, on_activate: Callable[[], None]):
        self.__color = Color
        self.__on_activate = on_activate

    def notify(self) -> None:
        self.__on_activate()
        print("activated i was")


class MidiButton:
    def __init__(self, ident: int, name: str, midi_type: Type, default_state: State, active_state: State, stay_active: bool = False):
        self.__ident = ident
        self.name = name
        self.__midi_type = midi_type
        self.__default_state = default_state
        self.__active_state = active_state
        self.__current_state = default_state
        self.__stay_active = stay_active

    def activate(self):
        self.__current_state = self.__active_state
        self.__current_state.notify()
        if not self.__stay_active:
            self.deactivate()

    def deactivate(self):
        self.__current_state = self.__default_state
        self.__current_state.notify()
