Development reference
-----------------------

This page contains all information over the internal elements of gyes. This is most likely only interesting for the development of gyes. For all other cases there is the more end-user-style :ref:`API Reference <api>`.


Data
*****


.. automodule:: gyes.data
    :members:
    :private-members:
    :noindex:


OSC Module
***********


OSC
.....


.. automodule:: gyes.osc
    :members:
    :private-members:
    :noindex:


Hooks
......


.. automodule:: gyes.osc.hooks
    :members:
    :private-members:
    :noindex:


