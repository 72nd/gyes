Welcome to gyes's documentation!
================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   api
   dev


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
