import logging

from gyes import Gyes, OSC
from gyes.data import Values
from gyes.osc import OSCMonitor


def test(address: str, values: Values):
    print("hello")


def regex_test(address, values, matches):
    print("regex: address «{}», matches: {}".format(address, matches))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    mo = Gyes()
    osc = OSC(
        host="127.0.0.1",
        port_in=9000,
        dst_host="127.0.0.1",
        dst_port=9010
    )
    osc.add_hook("/empty", test)
    osc.add_hook("/float", test, values=[0.0, 1.0])
    osc.add_regex_hook("/player/(.*)/play", regex_test)
    mo.add_osc(osc)

    osc2 = OSCMonitor(
        port_in=9001
    )
    mo.add_osc(osc2)

    # midi = MIDI(
    #    input_name="Launchpad Pro:Launchpad Pro MIDI 2 24:1"
    # )
    # mo.add_midi(midi)

    mo.serve()
